import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './css/style.css'

import { Hello } from './components/Hello';

const container = <Hello compiler="TypeScript" framework="React" />;

ReactDOM.render(
    container,
    document.getElementById('root')
)