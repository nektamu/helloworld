import * as React from 'react';

export interface HelloProps { compiler: string; framework: string; }

export class Hello extends React.Component<HelloProps, { hide: boolean }> {
    constructor(props: HelloProps) {
        super(props);
        
        this.state = {
            hide: false
        }

        this.hideText = this.hideText.bind(this)
    }
    hideText() {
        console.log('work')
        this.setState({ hide: !this.state.hide })
    }
    render() {
        return <h1 onClick={this.hideText} className={this.state.hide  ? 'hidden' : ''}>Hello from {this.props.compiler} and {this.props.framework}</h1>;
    }
}